import pandas as pd
import random
from collections import defaultdict
import time

clasificacion = pd.read_excel('clasificacion_laliga.xlsx', sheet_name='tabla', header=0, index_col=0)
enfrentamientos = pd.read_excel('clasificacion_laliga.xlsx', sheet_name='enfrentamientos', header=0)

num_simulaciones = 100

#V,E,D
sistema_puntos = [3,1,0]

def resultado(goles_local, goles_visitante):
    resultados = ['V', 'E', 'D']

    if goles_local > goles_visitante:
        return resultados[0]
    elif goles_visitante > goles_local:
        return resultados[2]
    else:
        return resultados[1]


def asignarPuntos(resultado):
    if resultado == 'V':
        return (sistema_puntos[0], sistema_puntos[2])
    elif resultado == 'E':
        return (sistema_puntos[1],sistema_puntos[1])
    else:
        return (sistema_puntos[2], sistema_puntos[0])


def resultado_CampeonLiga(clasificacion):
    return clasificacion.index[0]

def resultado_Champions(clasificacion):
    return clasificacion.index[:4]

def resultado_EuropaLeague(clasificacion):
    return clasificacion.index[4:6]

def resultado_Descenso(clasificacion):
    return clasificacion.index[-3:]

def asignarGoles():

    goles_local = random.choice(range(0,5))
    goles_visitante = random.choice(range(0,5))

    return (goles_local,goles_visitante)

def desempate_golaverage(nombre_equipo1, nombre_equipo2, clasificacion_actual):

    dg1 = clasificacion_actual.loc[nombre_equipo1]['DG']
    dg2 = clasificacion_actual.loc[nombre_equipo2]['DG']

    if dg1 > dg2:
        return nombre_equipo1
    elif dg2 > dg1:
        return nombre_equipo2
    else:
        goles_a_favor1 = clasificacion_actual.loc[nombre_equipo1]['GF']
        goles_a_favor2 = clasificacion_actual.loc[nombre_equipo2]['GF']

        if goles_a_favor1 > goles_a_favor2:
            return nombre_equipo1
        else:
            return nombre_equipo2

def desempateDireto(nombre_equipo1, nombre_equipo2, clasificacion_actual):

    resultado1 = registro_partidos.at[nombre_equipo1, nombre_equipo2]
    resultado2 = registro_partidos.at[nombre_equipo2, nombre_equipo1]


    resultado_global_equipo1 = resultado1[0] + resultado2[1]
    resultado_global_equipo2 = resultado2[0] + resultado1[1]

    if resultado_global_equipo1 > resultado_global_equipo2:
        return nombre_equipo1
    elif resultado_global_equipo2 > resultado_global_equipo1:
        return nombre_equipo2
    else:
        return desempate_golaverage(nombre_equipo1, nombre_equipo2, clasificacion_actual)
    

def equipos_empatados(clasificacion):
    equipos_empatados = []

    # Iterar sobre los equipos ordenados por puntos
    for idx, equipo1 in enumerate(clasificacion.index):
        # Comparar el equipo1 con los equipos que están por debajo en la clasificación
        for equipo2 in clasificacion.index[idx+1:]:
            # Comprobar si tienen el mismo número de puntos
            if clasificacion.loc[equipo1, 'Pts'] == clasificacion.loc[equipo2, 'Pts']:
                equipos_empatados.append((equipo1, equipo2))

    return equipos_empatados

def reordenacionClasificacion(clasificacion_copia, empatados):
    # Resetear el índice y guardar la columna "Equipo"
    clasificacion_copia['Equipo'] = clasificacion_copia.index
    
    # Desempatar y reordenar la clasificación copia
    for equipo1, equipo2 in empatados:
        ganador = desempateDireto(equipo1, equipo2, clasificacion_actual=clasificacion_copia)
        if ganador == equipo1:
            temp = clasificacion_copia.loc[equipo2].copy()
            clasificacion_copia.loc[equipo2] = clasificacion_copia.loc[equipo1]
            clasificacion_copia.loc[equipo1] = temp
        elif ganador == equipo2:
            temp = clasificacion_copia.loc[equipo1].copy()
            clasificacion_copia.loc[equipo1] = clasificacion_copia.loc[equipo2]
            clasificacion_copia.loc[equipo2] = temp

    # Restaurar "Equipo" como el índice
    clasificacion_copia.set_index('Equipo', inplace=True)
    return clasificacion_copia


registro_partidos = pd.DataFrame(index=range(20), columns=range(20))
registro_partidos = registro_partidos.applymap(lambda x: (0, 0))
registro_partidos.index = clasificacion.index
registro_partidos.columns = clasificacion.index

equipos_campeones = defaultdict(int)
equipos_champions = defaultdict(int)
equipos_europaLeague = defaultdict(int)
equipos_descensos = defaultdict(int)

start_time = time.time()

for _ in range(0,num_simulaciones):

    clasificacion_copia = clasificacion.copy()
    enfrentamientos_copia = enfrentamientos.copy()

    for equipo in clasificacion.index:
        rivales = enfrentamientos_copia[equipo].dropna()

        for rival in rivales:

            if rival == None:
                continue

            goles = asignarGoles()

            registro_partidos.at[equipo,rival] = goles
            resultado_equipos = resultado(goles[0],goles[1])
            puntuacion = asignarPuntos(resultado_equipos)

            clasificacion_copia.loc[equipo]['Pts'] += puntuacion[0]
            clasificacion_copia.loc[equipo]['PJ'] += 1
            clasificacion_copia.loc[equipo]['GF'] += goles[0]
            clasificacion_copia.loc[equipo]['GC'] += goles[1]
            clasificacion_copia.loc[equipo]['DG'] += (goles[0] - goles[1])

            clasificacion_copia.loc[rival]['Pts'] += puntuacion[1]
            clasificacion_copia.loc[rival]['PJ'] += 1
            clasificacion_copia.loc[rival]['GF'] += goles[1]
            clasificacion_copia.loc[rival]['GC'] += goles[0]
            clasificacion_copia.loc[rival]['DG'] += (goles[1] - goles[0])

            enfrentamientos_copia.loc[enfrentamientos_copia[rival] == equipo, rival] = None
    
    
    clasificacion_copia = clasificacion_copia.sort_values(by='Pts', ascending=False)
    empatados = equipos_empatados(clasificacion_copia)
    clasificacion_copia = reordenacionClasificacion(clasificacion_copia, empatados)

    campeon = resultado_CampeonLiga(clasificacion=clasificacion_copia)
    equipos_campeones[campeon] += 1

    champions = resultado_Champions(clasificacion=clasificacion_copia)
    for equipo_champions in champions:
        equipos_champions[equipo_champions] += 1

    europaLeague = resultado_EuropaLeague(clasificacion=clasificacion_copia)
    for equipo_europaLeague in europaLeague:
        equipos_europaLeague[equipo_europaLeague] += 1

    descenso = resultado_Descenso(clasificacion=clasificacion_copia)
    for equipo_descenso in descenso:
        equipos_descensos[equipo_descenso] += 1

print("Probabilidades de Ganar la Liga\n")
for campeon in equipos_campeones:
    print(campeon + '-' + str(round(100*(equipos_campeones[campeon]/num_simulaciones),2)) + "%")


print("Probabilidades de entrar en Champions\n")
for champions in equipos_champions:
    print(champions + '-' + str(round(100*(equipos_champions[champions]/num_simulaciones),2)) + "%")


print("Probabilidades de entrar en Europa League\n")
for europaLeague in equipos_europaLeague:
    print(europaLeague + '-' + str(round(100*(equipos_europaLeague[europaLeague]/num_simulaciones),2)) + "%")

print("Probabilidades de Descender\n")
for descenso in equipos_descensos:
    print(descenso + '-' + str(round(100*(equipos_descensos[descenso]/num_simulaciones),2)) + "%")


print("--- %s seconds ---" % (time.time() - start_time))