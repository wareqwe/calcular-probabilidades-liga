import pandas as pd
import random
from collections import defaultdict
import warnings
import time
import numpy as np

# Ignorar la advertencia específica relacionada con FutureWarning
warnings.filterwarnings("ignore", message="The default value of numeric_only in DataFrameGroupBy.sum is deprecated.")
# Ignorar la advertencia específica relacionada con FutureWarning
warnings.filterwarnings("ignore", message="Not prepending group keys to the result index of transform-like apply.")

clasificacion = pd.read_excel('clasificacion_laliga.xlsx', sheet_name='tabla', header=0, index_col=0)
enfrentamientos_lista = pd.read_excel('clasificacion_laliga.xlsx', sheet_name='enfrentamientos_lista', header=0)

num_simulaciones = 100

#V,E,D
sistema_puntos = [3,1,0]

def resultado(goles_local, goles_visitante):
    resultados = ['V', 'E', 'D']

    if goles_local > goles_visitante:
        return resultados[0]
    elif goles_visitante > goles_local:
        return resultados[2]
    else:
        return resultados[1]


def asignarPuntos(resultado):
    if resultado == 'V':
        return (sistema_puntos[0], sistema_puntos[2])
    elif resultado == 'E':
        return (sistema_puntos[1],sistema_puntos[1])
    else:
        return (sistema_puntos[2], sistema_puntos[0])


def resultado_CampeonLiga(clasificacion):
    return clasificacion.nlargest(1, 'Pts').index[0]

def resultado_Champions(clasificacion):
    return clasificacion.nlargest(4, 'Pts').index

def resultado_EuropaLeague(clasificacion):
    return clasificacion.nlargest(6, 'Pts').index[4:6]

def resultado_Descenso(clasificacion):
    return clasificacion.nsmallest(3, 'Pts').index

def asignarGoles():

    goles_local = random.choice(range(0,5))
    goles_visitante = random.choice(range(0,5))

    return (goles_local,goles_visitante)

def desempate_golaverage(nombre_equipo1, nombre_equipo2, clasificacion_actual):

    dg1 = clasificacion_actual.loc[nombre_equipo1]['DG']
    dg2 = clasificacion_actual.loc[nombre_equipo2]['DG']

    if dg1 > dg2:
        return nombre_equipo1
    elif dg2 > dg1:
        return nombre_equipo2
    else:
        goles_a_favor1 = clasificacion_actual.loc[nombre_equipo1]['GF']
        goles_a_favor2 = clasificacion_actual.loc[nombre_equipo2]['GF']

        if goles_a_favor1 > goles_a_favor2:
            return nombre_equipo1
        else:
            return nombre_equipo2

def desempateDireto(nombre_equipo1, nombre_equipo2, clasificacion_actual):

    resultado1 = registro_partidos.at[nombre_equipo1, nombre_equipo2]
    resultado2 = registro_partidos.at[nombre_equipo2, nombre_equipo1]


    resultado_global_equipo1 = resultado1[0] + resultado2[1]
    resultado_global_equipo2 = resultado2[0] + resultado1[1]

    if resultado_global_equipo1 > resultado_global_equipo2:
        return nombre_equipo1
    elif resultado_global_equipo2 > resultado_global_equipo1:
        return nombre_equipo2
    else:
        return desempate_golaverage(nombre_equipo1, nombre_equipo2, clasificacion_actual)
    

def equipos_empatados(clasificacion):
    # Crear una columna 'Pts' duplicada para comparación
    clasificacion['Pts_dup'] = clasificacion['Pts']

    # Generar una matriz booleana que indica qué equipos tienen el mismo número de puntos
    same_points_matrix = clasificacion['Pts'].values[:, None] == clasificacion['Pts_dup'].values

    # Crear una matriz triangular superior para evitar duplicados
    same_points_matrix = np.triu(same_points_matrix, k=1)

    # Obtener las coordenadas de los valores True en la matriz
    idx = np.where(same_points_matrix)

    # Obtener los nombres de los equipos empatados
    equipos_empatados = [(clasificacion.index[i], clasificacion.index[j]) for i, j in zip(*idx)]

    # Eliminar la columna duplicada
    clasificacion.drop(columns=['Pts_dup'], inplace=True)

    return equipos_empatados

def reordenacionClasificacion(clasificacion_copia, empatados):
    # Resetear el índice y guardar la columna "Equipo"
    clasificacion_copia['Equipo'] = clasificacion_copia.index
    
    # Desempatar y reordenar la clasificación copia
    for equipo1, equipo2 in empatados:
        ganador = desempateDireto(equipo1, equipo2, clasificacion_actual=clasificacion_copia)
        if ganador == equipo1:
            clasificacion_copia.loc[[equipo1, equipo2]] = clasificacion_copia.loc[[equipo2, equipo1]].values
        elif ganador == equipo2:
            clasificacion_copia.loc[[equipo1, equipo2]] = clasificacion_copia.loc[[equipo1, equipo2]].values

    # Restaurar "Equipo" como el índice
    clasificacion_copia.set_index('Equipo', inplace=True)
    return clasificacion_copia


registro_partidos = pd.DataFrame(index=range(20), columns=range(20))
registro_partidos = registro_partidos.applymap(lambda x: (0, 0))
registro_partidos.index = clasificacion.index
registro_partidos.columns = clasificacion.index

equipos_campeones = defaultdict(int)
equipos_champions = defaultdict(int)
equipos_europaLeague = defaultdict(int)
equipos_descensos = defaultdict(int)

def asignarPuntos(goles):

    goles_local = goles[0]
    goles_visitante = goles[1]

    if goles_local > goles_visitante:
        return (3, 0)
    elif goles_local < goles_visitante:
        return (0, 3)
    else:
        return (1, 1)


start_time = time.time()

for _ in range(num_simulaciones):
    clasificacion_copia = clasificacion.copy()
    enfrentamientos_copia = enfrentamientos_lista.copy()

    resultados_goles = enfrentamientos_copia.apply(lambda row: asignarGoles(), axis=1)
    puntos_equipo = resultados_goles.map(asignarPuntos)
    
    # Crear un DataFrame temporal con los puntos asignados a cada equipo y rival
    temp_df_puntos = pd.DataFrame(puntos_equipo.tolist(), columns=['Puntos Equipo', 'Puntos Rival'], index=puntos_equipo.index)
    temp_df_puntos['Equipo'] = enfrentamientos_copia['Equipo']
    temp_df_puntos['Rival'] = enfrentamientos_copia['Rival']

    # Crear un DataFrame temporal con los goles a favor y en contra de cada equipo y rival
    temp_df_goles = pd.DataFrame(resultados_goles.tolist(), columns=['Goles Equipo', 'Goles Rival'], index=resultados_goles.index)
    temp_df_goles['Equipo'] = enfrentamientos_copia['Equipo']
    temp_df_goles['Rival'] = enfrentamientos_copia['Rival']

    # Sumar los puntos asignados a cada equipo y rival
    puntos_por_equipo = temp_df_puntos.groupby('Equipo').sum()
    puntos_por_rival = temp_df_puntos.groupby('Rival').sum()

    # Sumar los goles a favor y en contra de cada equipo y rival
    goles_por_equipo = temp_df_goles.groupby('Equipo').sum()
    goles_por_rival = temp_df_goles.groupby('Rival').sum()

    # Actualizar la tabla de clasificación
    clasificacion_copia.loc[puntos_por_equipo.index, 'Pts'] += puntos_por_equipo['Puntos Equipo']
    clasificacion_copia.loc[puntos_por_rival.index, 'Pts'] += puntos_por_rival['Puntos Rival']
    
    clasificacion_copia.loc[goles_por_equipo.index, 'GF'] += goles_por_equipo['Goles Equipo']
    clasificacion_copia.loc[goles_por_equipo.index, 'GC'] += goles_por_equipo['Goles Rival']
    clasificacion_copia.loc[goles_por_rival.index, 'GF'] += goles_por_rival['Goles Rival']
    clasificacion_copia.loc[goles_por_rival.index, 'GC'] += goles_por_rival['Goles Equipo']

    empatados = equipos_empatados(clasificacion_copia)
    clasificacion_copia = reordenacionClasificacion(clasificacion_copia, empatados)

    campeon = resultado_CampeonLiga(clasificacion=clasificacion_copia)
    equipos_campeones[campeon] += 1

    champions = resultado_Champions(clasificacion=clasificacion_copia)
    for equipo_champions in champions:
        equipos_champions[equipo_champions] += 1

    europaLeague = resultado_EuropaLeague(clasificacion=clasificacion_copia)
    for equipo_europaLeague in europaLeague:
        equipos_europaLeague[equipo_europaLeague] += 1

    descenso = resultado_Descenso(clasificacion=clasificacion_copia)
    for equipo_descenso in descenso:
        equipos_descensos[equipo_descenso] += 1

print("--- %s seconds ---" % (time.time() - start_time))

# Restaurar el comportamiento predeterminado de las advertencias
warnings.filterwarnings("default")