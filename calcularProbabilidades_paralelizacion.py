import pandas as pd
import random
from collections import defaultdict
import warnings
import time
from concurrent.futures import ProcessPoolExecutor, as_completed
import numpy as np
from multiprocessing import Queue
import atexit
import multiprocessing

import psutil
import os

pid = os.getpid()
p = psutil.Process(pid)
p.nice(psutil.HIGH_PRIORITY_CLASS)

# Registrar una función para limpiar los recursos al finalizar el programa
@atexit.register
def clean_up():
    multiprocessing.get_all_start_methods()  # Esto desencadena la limpieza de los recursos

# Ignorar la advertencia específica relacionada con FutureWarning
warnings.filterwarnings("ignore", message="The default value of numeric_only in DataFrameGroupBy.sum is deprecated.")
# Ignorar la advertencia específica relacionada con FutureWarning
warnings.filterwarnings("ignore", message="Not prepending group keys to the result index of transform-like apply.")

clasificacion = pd.read_excel('clasificacion_laliga.xlsx', sheet_name='tabla', header=0, index_col=0)
enfrentamientos_lista = pd.read_excel('clasificacion_laliga.xlsx', sheet_name='enfrentamientos_lista', header=0)

# V,E,D
sistema_puntos = [3, 1, 0]

def resultado(goles_local, goles_visitante):
    resultados = ['V', 'E', 'D']

    if goles_local > goles_visitante:
        return resultados[0]
    elif goles_visitante > goles_local:
        return resultados[2]
    else:
        return resultados[1]

def asignarPuntos(resultado):
    if resultado == 'V':
        return (sistema_puntos[0], sistema_puntos[2])
    elif resultado == 'E':
        return (sistema_puntos[1], sistema_puntos[1])
    else:
        return (sistema_puntos[2], sistema_puntos[0])

def resultado_CampeonLiga(clasificacion):
    return clasificacion.index[0]

def resultado_Champions(clasificacion):
    return clasificacion.index[:4]

def resultado_EuropaLeague(clasificacion):
    return clasificacion.index[4:6] #desde el 4 al 5 (quinta y sexta posición)

def resultado_Descenso(clasificacion):
    return clasificacion.index[-3:]

def asignarGoles():
    goles_local = random.choice(range(0, 5))
    goles_visitante = random.choice(range(0, 5))
    return (goles_local, goles_visitante)

def desempate_golaverage(nombre_equipo1, nombre_equipo2, clasificacion_actual):
    dg1 = clasificacion_actual.loc[nombre_equipo1]['DG']
    dg2 = clasificacion_actual.loc[nombre_equipo2]['DG']

    if dg1 > dg2:
        return nombre_equipo1
    elif dg2 > dg1:
        return nombre_equipo2
    else:
        goles_a_favor1 = clasificacion_actual.loc[nombre_equipo1]['GF']
        goles_a_favor2 = clasificacion_actual.loc[nombre_equipo2]['GF']

        if goles_a_favor1 > goles_a_favor2:
            return nombre_equipo1
        else:
            return nombre_equipo2

def desempateDireto(nombre_equipo1, nombre_equipo2, clasificacion_actual):
    resultado1 = registro_partidos.at[nombre_equipo1, nombre_equipo2]
    resultado2 = registro_partidos.at[nombre_equipo2, nombre_equipo1]

    resultado_global_equipo1 = resultado1[0] + resultado2[1]
    resultado_global_equipo2 = resultado2[0] + resultado1[1]

    if resultado_global_equipo1 > resultado_global_equipo2:
        return nombre_equipo1
    elif resultado_global_equipo2 > resultado_global_equipo1:
        return nombre_equipo2
    else:
        return desempate_golaverage(nombre_equipo1, nombre_equipo2, clasificacion_actual)

def equipos_empatados(clasificacion):
    # Crear una columna 'Pts' duplicada para comparación
    clasificacion['Pts_dup'] = clasificacion['Pts']

    # Generar una matriz booleana que indica qué equipos tienen el mismo número de puntos
    same_points_matrix = clasificacion['Pts'].values[:, None] == clasificacion['Pts_dup'].values

    # Crear una matriz triangular superior para evitar duplicados
    same_points_matrix = np.triu(same_points_matrix, k=1)

    # Obtener las coordenadas de los valores True en la matriz
    idx = np.where(same_points_matrix)

    # Obtener los nombres de los equipos empatados
    equipos_empatados = [(clasificacion.index[i], clasificacion.index[j]) for i, j in zip(*idx)]

    # Eliminar la columna duplicada
    clasificacion.drop(columns=['Pts_dup'], inplace=True)

    return equipos_empatados

def reordenacionClasificacion(clasificacion_copia, empatados):
    # Resetear el índice y guardar la columna "Equipo"
    clasificacion_copia['Equipo'] = clasificacion_copia.index
    
    # Desempatar y reordenar la clasificación copia
    for equipo1, equipo2 in empatados:
        ganador = desempateDireto(equipo1, equipo2, clasificacion_actual=clasificacion_copia)
        if ganador == equipo1:
            clasificacion_copia.loc[[equipo1, equipo2]] = clasificacion_copia.loc[[equipo2, equipo1]].values
        elif ganador == equipo2:
            clasificacion_copia.loc[[equipo1, equipo2]] = clasificacion_copia.loc[[equipo1, equipo2]].values

    # Restaurar "Equipo" como el índice
    clasificacion_copia.set_index('Equipo', inplace=True)
    return clasificacion_copia

registro_partidos = pd.DataFrame(index=range(20), columns=range(20))
registro_partidos = registro_partidos.applymap(lambda x: (0, 0))
registro_partidos.index = clasificacion.index
registro_partidos.columns = clasificacion.index

def asignarPuntos(goles):
    goles_local = goles[0]
    goles_visitante = goles[1]

    if goles_local > goles_visitante:
        return (3, 0)
    elif goles_local < goles_visitante:
        return (0, 3)
    else:
        return (1, 1)

def simular_una_vez(_):
    #print("--- 1 --- Estoy simulando una clasificación")
    try:
        clasificacion_copia = clasificacion.copy()
        enfrentamientos_copia = enfrentamientos_lista.copy()

        resultados_goles = enfrentamientos_copia.apply(lambda row: asignarGoles(), axis=1)
        puntos_equipo = resultados_goles.map(asignarPuntos)

        temp_df_puntos = pd.DataFrame(puntos_equipo.tolist(), columns=['Puntos Equipo', 'Puntos Rival'], index=puntos_equipo.index)
        temp_df_puntos['Equipo'] = enfrentamientos_copia['Equipo']
        temp_df_puntos['Rival'] = enfrentamientos_copia['Rival']

        temp_df_goles = pd.DataFrame(resultados_goles.tolist(), columns=['Goles Equipo', 'Goles Rival'], index=resultados_goles.index)
        temp_df_goles['Equipo'] = enfrentamientos_copia['Equipo']
        temp_df_goles['Rival'] = enfrentamientos_copia['Rival']

        puntos_por_equipo = temp_df_puntos.groupby('Equipo').sum()
        puntos_por_rival = temp_df_puntos.groupby('Rival').sum()

        goles_por_equipo = temp_df_goles.groupby('Equipo').sum()
        goles_por_rival = temp_df_goles.groupby('Rival').sum()

        clasificacion_copia.loc[puntos_por_equipo.index, 'Pts'] += puntos_por_equipo['Puntos Equipo']
        clasificacion_copia.loc[puntos_por_rival.index, 'Pts'] += puntos_por_rival['Puntos Rival']

        clasificacion_copia.loc[goles_por_equipo.index, 'GF'] += goles_por_equipo['Goles Equipo']
        clasificacion_copia.loc[goles_por_equipo.index, 'GC'] += goles_por_equipo['Goles Rival']
        clasificacion_copia.loc[goles_por_rival.index, 'GF'] += goles_por_rival['Goles Rival']
        clasificacion_copia.loc[goles_por_rival.index, 'GC'] += goles_por_rival['Goles Equipo']

        equiposEmpatados_puntos = equipos_empatados(clasificacion_copia)
        clasificacion_copia = reordenacionClasificacion(clasificacion_copia, equiposEmpatados_puntos)

        clasificacion_copia = clasificacion_copia.sort_values(by='Pts', ascending=False)

        return clasificacion_copia
    except Exception as e:
        print(f"Error en la simulación: {str(e)}")
        return None

def procesar_campeones(clasificacion_copia, equipos_campeones):
    try:
        campeon = resultado_CampeonLiga(clasificacion=clasificacion_copia)
        equipos_campeones[campeon] += 1
    except Exception as e:
        print(f"Error al procesar campeón: {str(e)}")

def procesar_champions(clasificacion_copia, equipos_champions):
    try:
        champions = resultado_Champions(clasificacion=clasificacion_copia)
        for equipo_champions in champions:
            equipos_champions[equipo_champions] += 1
    except Exception as e:
        print(f"Error al procesar Champions League: {str(e)}")

def procesar_europa_league(clasificacion_copia, equipos_europa):
    try:
        europa_league = resultado_EuropaLeague(clasificacion=clasificacion_copia)
        for equipo_europa_league in europa_league:
            equipos_europa[equipo_europa_league] += 1
    except Exception as e:
        print(f"Error al procesar Europa League: {str(e)}")

def procesar_descensos(clasificacion_copia, equipos_descensos):
    try:
        descensos = resultado_Descenso(clasificacion=clasificacion_copia)
        for equipo_descenso in descensos:
            equipos_descensos[equipo_descenso] += 1
    except Exception as e:
        print(f"Error al procesar descensos: {str(e)}")

if __name__ == '__main__':
    start_time = time.time()

    try:
        num_simulaciones = 100000
        equipos_campeones = defaultdict(int)
        equipos_champions = defaultdict(int)
        equipos_europa = defaultdict(int)
        equipos_descensos = defaultdict(int)

        with ProcessPoolExecutor(10) as executor:
            futures = []
            for _ in range(num_simulaciones):
                futures.append(executor.submit(simular_una_vez, None))

            # Procesar los resultados tan pronto como estén disponibles
            for future in as_completed(futures):
                clasificacion_copia = future.result()
                procesar_campeones(clasificacion_copia, equipos_campeones)
                procesar_champions(clasificacion_copia, equipos_champions)
                procesar_europa_league(clasificacion_copia, equipos_europa)
                procesar_descensos(clasificacion_copia, equipos_descensos)

    except Exception as e:
        print(f"Error en la ejecución: {e}")

    print("--- %s seconds ---" % (time.time() - start_time))

    # Mostrar los resultados después de que se completaron todas las simulaciones y procesamientos
    print("Probabilidades de Ganar la Liga\n")
    for campeon in equipos_campeones:
        print(campeon + '-' + str(round(100 * (equipos_campeones[campeon] / num_simulaciones), 2)) + "%")

    print("Probabilidades de entrar en Champions\n")
    for champions in equipos_champions:
        print(champions + '-' + str(round(100 * (equipos_champions[champions] / num_simulaciones), 2)) + "%")

    print("Probabilidades de entrar en Europa League\n")
    for europaLeague in equipos_europa:
        print(europaLeague + '-' + str(round(100 * (equipos_europa[europaLeague] / num_simulaciones), 2)) + "%")

    print("Probabilidades de Descender\n")
    for descenso in equipos_descensos:
        print(descenso + '-' + str(round(100 * (equipos_descensos[descenso] / num_simulaciones), 2)) + "%")
